import java.awt.*;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Path2D;
import java.awt.geom.Point2D;

/**
 * Created by Daniel on 12/3/15.
 */
public class Robot {
    Point p;
    public int direction; // in degrees, 0-359
    public boolean pendown;

    boolean right;
    boolean left;
    boolean up;
    boolean down;

    class Vector {
        double xComp;
        double yComp;
    }

    class Point {
        double x;
        double y;
        double diameter;

        public Point() {
        }

        public Point(double x, double y, int diameter) {
            this.x = x;
            this.y = y;
            this.diameter = diameter ;
        }
    }

    Path2D.Double path;

    public Robot() {
        path = new Path2D.Double();
        p = new Point();
        pendown = true;
        direction = 0; // facing right (0 degrees)
    }

    public Robot(double x, double y, double diameter) {
        this();
        p.x = x;
        p.y = y;
        p.diameter = diameter;
        Point2D.Double center = getCenter();
        path.moveTo(center.getX(), center.getY());
    }

    public void clearPath() {
        path = new Path2D.Double();
        Point2D.Double center = getCenter();
        path.moveTo(center.getX(), center.getY());
    }

    public void move(int amt) {
        p.x = p.x + amt * Math.cos(Math.toRadians(direction));
        p.y = p.y + amt * Math.sin(Math.toRadians(direction));

        if (pendown) {
            Point2D.Double center = getCenter();
            path.lineTo(center.getX(), center.getY());
        }
    }

    public void turn(int degrees) {
        direction = (direction + degrees) % 360;
    }

    public double getX() {
        return p.x;
    }

    public void setX(double x) {
        path.closePath();;
        p.x = x;
    }

    public double getY() {
        return p.y;
    }

    public void setY(double y) {
        path.closePath();
        p.y = y;
    }

    public double getDiameter() {
        return p.diameter;
    }

    public void setDiameter(double diameter) {
        p.diameter = diameter;
    }

    public void penDown() {
        pendown = true;
    }

    public void penUp() {
        pendown = false;
    }

    public void draw(Graphics2D g2) {
        if (right) p.x += 1;
        if (left) p.x -= 1;
        if (up) p.y -= 1;
        if (down) p.y += 1;

        g2.fill(new Ellipse2D.Double(p.x, p.y, p.diameter, p.diameter));
        g2.setStroke(new BasicStroke(3));
        g2.draw(path);

        Point2D.Double center = getCenter();
        if (pendown) {
            path.lineTo(center.getX(), center.getY());
        } else {
            path.moveTo(center.getX(), center.getY());
        }
    }

    private Point2D.Double getCenter() {
        return new Point2D.Double(p.x + (p.diameter / 2), p.y + (p.diameter / 2));
    }
}

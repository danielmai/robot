import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Date;

/**
 * Created by Daniel on 11/22/15.
 *
 * Taken from Chapter 4, CS 151 lecture notes
 * Modified to have a slider that adjusts the timer delay
 */
public class TimerTester {

    public static void main(String[] args) {
        JFrame frame = new JFrame();

        final int FIELD_WIDTH = 20;
        final JTextField textField = new JTextField(FIELD_WIDTH);
        final JSlider slider = new JSlider(1, 10);
        slider.setMajorTickSpacing(1);
        slider.setPaintLabels(true);
        slider.setPaintTicks(true);
        slider.setSnapToTicks(true);

        frame.setLayout(new FlowLayout());
        frame.add(textField);
        frame.add(slider);

        final int DELAY = 1000; // in ms

        final Timer t = new Timer(DELAY, e -> {
            Date now = new Date();
            textField.setText(now.toString());
        });
        t.start();

        slider.addChangeListener(e -> {
            JSlider source = (JSlider) e.getSource();
            if (!source.getValueIsAdjusting()) {
                int n = source.getValue();
                int delay = n * 1000;
                t.setDelay(delay);
                t.restart();
            }
        });

        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);

    }
}

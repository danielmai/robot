import javax.swing.*;

/**
 * Created by Daniel on 11/22/15.
 */
public class Frame {
    public static void main(String[] args) {
        JFrame frame = new JFrame("Robot Window");
        frame.setSize(400, 400);

        Robot robot = new Robot(0, 0, 10);

        RobotComponent rc = new RobotComponent(frame, robot);
        rc.setOpaque(true);
        rc.setBounds(0, 0, 400, 400);
        frame.add(rc);

        final int DELAY = 5;
        Timer t = new Timer(DELAY, e -> rc.repaint());
        t.start();
/*
        robot.setX(200);
        robot.setY(200);

        for (int i = 0; i < 36; i++) {
            robot.move(10);
            robot.turn(10);
        }

        robot.penUp();

        robot.setX(100);
        robot.setY(100);

        robot.penDown();

        for (int i = 0; i < 36; i++) {
            robot.move(10);
            robot.turn(10);
        }*/


        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setVisible(true);
    }
}

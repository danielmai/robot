import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

/**
 * Created by Daniel on 11/22/15.
 */
public class RobotComponent extends JPanel {

    Robot robot;

    public RobotComponent(JFrame frame, Robot robot) {
        this.robot = robot;
        frame.addKeyListener(new KeyAdapter() {
            @Override
            public void keyPressed(KeyEvent e) {
                if (e.getKeyCode() == KeyEvent.VK_LEFT) robot.left = true;
                if (e.getKeyCode() == KeyEvent.VK_RIGHT) robot.right = true;
                if (e.getKeyCode() == KeyEvent.VK_UP) robot.up = true;
                if (e.getKeyCode() == KeyEvent.VK_DOWN) robot.down = true;
                if (e.getKeyCode() == KeyEvent.VK_C) robot.clearPath();
                if (e.getKeyCode() == KeyEvent.VK_SPACE) if (robot.pendown) robot.penUp(); else robot.penDown();
                if (e.getKeyCode() == KeyEvent.VK_A) System.out.println("robot.path = " + robot.path);
            }

            @Override
            public void keyReleased(KeyEvent e) {
                if (e.getKeyCode() == KeyEvent.VK_LEFT) robot.left = false;
                if (e.getKeyCode() == KeyEvent.VK_RIGHT) robot.right = false;
                if (e.getKeyCode() == KeyEvent.VK_UP) robot.up = false;
                if (e.getKeyCode() == KeyEvent.VK_DOWN) robot.down = false;
            }
        });
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        Graphics2D g2 = (Graphics2D) g;
        robot.draw(g2);
    }


}
